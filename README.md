# php-extended/php-ulid-object

A simple implementation of the php-extended/php-ulid-interface library.

![coverage](https://gitlab.com/php-extended/php-ulid-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-ulid-object/badges/master/coverage.svg?style=flat-square)


```
/!\ This library only works with 64-bits runtimes of php /!\
```


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-ulid-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\Ulid\Ulid;

$ulid = new Uuid(0x123456789, 0x123456789, 0x123456789);

$ulid->__toString(); // 0004HMASW904HMASW904HMASW9

```


To parse an ulid, do :

```php

use PhpExtended\Ulid\UlidParser;

$parser = new UlidParser();
$ulid = $parser->parse('<put here your ulid string>');
// $ulid instanceof UlidInterface

```

To create an ulid, do :

```php

use PhpExtended\Uuid\UlidRandomFactory;

$factory = new UlidRandomFactory();
$ulid = $factory->create(); // 01EKEWH5HYSHZPMYCGVT44YZJ6

```


## License

MIT (See [license file](LICENSE)).
