<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ulid\Ulid;
use PhpExtended\Ulid\UlidMonotonicFactory;
use PHPUnit\Framework\TestCase;

/**
 * UlidMonotonicFactoryTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ulid\UlidMonotonicFactory
 *
 * @internal
 *
 * @small
 */
class UlidMonotonicFactoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UlidMonotonicFactory
	 */
	protected UlidMonotonicFactory $_object;
	
	public function testItWorks() : void
	{
		$this->assertInstanceOf(Ulid::class, $this->_object->create());
	}
	
	public function testPerf() : void
	{
		foreach($this->_object->createIterator(1000) as $object)
		{
			// nothing to do
		}
		$this->assertTrue(true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UlidMonotonicFactory();
	}
	
}
