<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ulid\Ulid;
use PhpExtended\Ulid\UlidRandomFactory;
use PHPUnit\Framework\TestCase;

/**
 * UlidRandomFactory class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ulid\UlidRandomFactory
 *
 * @internal
 *
 * @small
 */
class UlidRandomFactoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UlidRandomFactory
	 */
	protected UlidRandomFactory $_object;
	
	public function testItWorks() : void
	{
		$this->assertInstanceOf(Ulid::class, $this->_object->create());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UlidRandomFactory();
	}
	
}
