<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Parser\ParseException;
use PhpExtended\Ulid\UlidParser;
use PHPUnit\Framework\TestCase;

/**
 * UlidParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ulid\UlidParser
 *
 * @internal
 *
 * @small
 */
class UlidParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var UlidParser
	 */
	protected UlidParser $_parser;
	
	public function testItWorks() : void
	{
		$ulid = $this->_parser->parse('01B8KYR6G8BC61CE8R6K2T16HY');
		$this->assertEquals('01B8KYR6G8BC61CE8R6K2T16HY', $ulid->__toString());
	}
	
	public function testNotEnoughChars() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('0123456789');
	}
	
	public function testTimestampOverflow() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('81B8KYR6G8BC61CE8R6K2T16HY');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new UlidParser();
	}
	
}
