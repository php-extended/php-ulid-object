<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ulid\Ulid;
use PHPUnit\Framework\TestCase;

/**
 * UlidTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ulid\Ulid
 *
 * @internal
 *
 * @small
 */
class UlidTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Ulid
	 */
	protected Ulid $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('0004HMASW9163PAGS106986X35', $this->_object->__toString());
	}
	
	public function testGetDatetime() : void
	{
		$dt = DateTimeImmutable::createFromFormat('!U.u', (string) (float) (0x123456789 / 1000));
		$this->assertEquals($dt, $this->_object->getDateTime());
	}
	
	public function testGetTimestamp() : void
	{
		$this->assertEquals(0x123456789, $this->_object->getTimestamp());
	}
	
	public function testGetRandomnessUp() : void
	{
		$this->assertEquals(0x987654321, $this->_object->getRandomnessUp());
	}
	
	public function testGetRandomnessDown() : void
	{
		$this->assertEquals(0x192837465, $this->_object->getRandomnessDown());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Ulid(0x123456789, 0x987654321, 0x192837465);
	}
	
}
