<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ulid;

use DateTimeImmutable;
use DateTimeInterface;

/**
 * Ulid class file.
 * 
 * This class is a simple implementation of the UlidInterface.
 * 
 * @author Anastaszor
 */
class Ulid implements UlidInterface
{
	
	/**
	 * The timestamp part.
	 * 
	 * @var integer
	 */
	protected int $_timestamp;
	
	/**
	 * The up part of randomness.
	 * 
	 * @var integer
	 */
	protected int $_randomnessUp;
	
	/**
	 * The down part of randomness.
	 * 
	 * @var integer
	 */
	protected int $_randomnessDown;
	
	/**
	 * Builds a new Ulid with its integer parts.
	 * 
	 * @param integer $timestamp [0, 2^48-1] unix time in milliseconds
	 * @param int $randomnessUp [0, 2^40-1]
	 * @param int $randomnessDown [0, 2^40-1]
	 */
	public function __construct(int $timestamp, int $randomnessUp, int $randomnessDown)
	{
		$this->_timestamp = $timestamp & 0x0000FFFFFFFFFFFF;
		$this->_randomnessUp = $randomnessUp & 0x000000FFFFFFFFFF;
		$this->_randomnessDown = $randomnessDown & 0x000000FFFFFFFFFF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->crokfordBase32($this->_timestamp, 10)
			  .$this->crokfordBase32($this->_randomnessUp, 8)
			  .$this->crokfordBase32($this->_randomnessDown, 8);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ulid\UlidInterface::getDateTime()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function getDateTime() : DateTimeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return DateTimeImmutable::createFromFormat('U.u', (string) (float) ($this->_timestamp / 1000));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ulid\UlidInterface::getTimestamp()
	 */
	public function getTimestamp() : int
	{
		return $this->_timestamp;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ulid\UlidInterface::getRandomnessUp()
	 */
	public function getRandomnessUp() : int
	{
		return $this->_randomnessUp;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ulid\UlidInterface::getRandomnessDown()
	 */
	public function getRandomnessDown() : int
	{
		return $this->_randomnessDown;
	}
	
	/**
	 * Encodes the given $int with crockford base32 encoding and expand the
	 * resulting number to $pad characters.
	 * 
	 * @param integer $int
	 * @param integer $pad
	 * @return string
	 */
	protected function crokfordBase32(int $int, int $pad) : string
	{
		$table = '0123456789ABCDEFGHJKMNPQRSTVWXYZ';
		
		$str = '';
		
		while(0 !== $int)
		{
			// consume bits from the int stream 5 at a time
			$slice = $int & 0x1F;
			$str = $table[$slice].$str;
			$int = $int >> 5;
		}
		
		return \str_pad($str, $pad, '0', \STR_PAD_LEFT);
	}
	
}
