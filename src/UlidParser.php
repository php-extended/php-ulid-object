<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ulid;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;
use PhpExtended\Parser\ParseException;

/**
 * UlidParser class file.
 *
 * This class is a parser for ulids.
 *
 * @author Anastaszor
 * @extends AbstractParserLexer<UlidInterface>
 */
class UlidParser extends AbstractParserLexer implements UlidParserInterface
{
	
	public const L_CROCKFORD_EXT = 1;
	
	/**
	 * Builds a new UuidParser with the given lexer config.
	 */
	public function __construct()
	{
		parent::__construct(UlidInterface::class);
		$this->_config->addMappings(LexerInterface::CLASS_ALNUM, self::L_CROCKFORD_EXT);
		$this->_config->addMerging(self::L_CROCKFORD_EXT, self::L_CROCKFORD_EXT, self::L_CROCKFORD_EXT);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : UlidInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\AbstractParserLexer::parseLexer()
	 */
	public function parseLexer(LexerInterface $lexer) : UlidInterface
	{
		$chainStr = '';
		$lexeme = null;
		
		foreach($lexer as $lexeme)
		{
			if(UlidParser::L_CROCKFORD_EXT === $lexeme->getCode())
			{
				$chainStr .= $lexeme->getData();
			}
			
			if(26 <= \mb_strlen($chainStr))
			{
				break;
			}
		}
		
		if(26 > \mb_strlen($chainStr))
		{
			$message = 'Failed to find 26 crockford-32 compatible characters in byte stream (only {k} found).';
			$context = ['{k}' => \mb_strlen($chainStr)];
			$offset = null === $lexeme ? 0 : $lexeme->getColumn();
			$data = null === $lexeme ? '' : $lexeme->getData();
			
			throw new ParseException(UlidInterface::class, $data, $offset, \strtr($message, $context));
		}
		
		$timestampStr = (string) \mb_substr($chainStr, 0, 10);
		$randomUpStr = (string) \mb_substr($chainStr, 10, 8);
		$randomDownStr = (string) \mb_substr($chainStr, 18, 8);
		$firstChar = $timestampStr[0] ?? '?'; // is ascii more than 7
		
		if(\ord($firstChar) > \ord('7'))
		{
			$message = 'Overflow in byte stream on datetime value, found {value}';
			$context = ['{value}' => $chainStr];
			$offset = 1;
			
			throw new ParseException(UlidInterface::class, $firstChar, $offset, \strtr($message, $context));
		}
		
		return new Ulid(
			$this->crockfordDecode($timestampStr),
			$this->crockfordDecode($randomUpStr),
			$this->crockfordDecode($randomDownStr),
		);
	}
	
	/**
	 * Gets data from the crockford encoded string with common projections
	 * (UlidParser error correcting code).
	 * 
	 * @param string $str
	 * @return integer
	 */
	protected function crockfordDecode(string $str) : int
	{
		$table = [
			'0' => 0, 'O' => 0, 'o' => 0,
			'1' => 1, 'I' => 1, 'i' => 1, 'l' => 1, 'L' => 1,
			'2' => 2,
			'3' => 3,
			'4' => 4,
			'5' => 5,
			'6' => 6,
			'7' => 7,
			'8' => 8,
			'9' => 9,
			'A' => 10, 'a' => 10,
			'B' => 11, 'b' => 11,
			'C' => 12, 'c' => 12,
			'D' => 13, 'd' => 13,
			'E' => 14, 'e' => 14,
			'F' => 15, 'f' => 15,
			'G' => 16, 'g' => 16,
			'H' => 17, 'h' => 17,
			'J' => 18, 'j' => 18,
			'K' => 19, 'k' => 19,
			'M' => 20, 'm' => 20,
			'N' => 21, 'n' => 21,
			'P' => 22, 'p' => 22,
			'Q' => 23, 'q' => 23,
			'R' => 24, 'r' => 24,
			'S' => 25, 's' => 25,
			'T' => 26, 't' => 26,
			'U' => 27, 'u' => 27, 'V' => 27, 'v' => 27,
			'W' => 28, 'w' => 28,
			'X' => 29, 'x' => 29,
			'Y' => 30, 'y' => 30,
			'Z' => 31, 'z' => 31,
		];
		
		$total = 0;
		
		for($i = 0; \mb_strlen($str) > $i; $i++)
		{
			$total = $total << 5;
			$lookup = $table[$str[$i]];
			$total += $lookup;
		}
		
		return $total;
	}
	
}
