<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ulid;

use PhpExtended\Factory\AbstractFactory;

/**
 * UlidMonotonicFactory class file.
 *
 * This factory generates ulids that counts from 0 to MAX on each millisecond.
 *
 * @author Anastaszor
 * @extends \PhpExtended\Factory\AbstractFactory<UlidInterface>
 */
class UlidMonotonicFactory extends AbstractFactory implements UlidFactoryInterface
{
	
	/**
	 * The current ms.
	 * 
	 * @var integer
	 */
	protected int $_currentMs = 0;
	
	/**
	 * The current count up.
	 * 
	 * @var integer
	 */
	protected int $_currentCountUp = 0;
	
	/**
	 * The current count down.
	 * 
	 * @var integer
	 */
	protected int $_currentCountDown = 0;
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Factory\FactoryInterface::create()
	 */
	public function create() : UlidInterface
	{
		$now = (int) (\microtime(true) * 1000.0);
		
		if($this->_currentMs === $now)
		{
			$this->_currentCountDown++;
			// @codeCoverageIgnoreStart
			if(0xFFFFFFFFFF < $this->_currentCountDown)
			{
				$this->_currentCountDown = 0;
				$this->_currentCountUp++;
			}
			// @codeCoverageIgnoreEnd
			
			return new Ulid($this->_currentMs, $this->_currentCountUp, $this->_currentCountDown);
		}
		
		$this->_currentMs = $now;
		$this->_currentCountDown = 0;
		$this->_currentCountUp = 0;
		
		return new Ulid($this->_currentMs, $this->_currentCountUp, $this->_currentCountDown);
	}
	
}
