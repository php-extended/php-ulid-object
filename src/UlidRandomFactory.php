<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ulid;

use PhpExtended\Factory\AbstractFactory;
use Random\RandomException;
use Throwable;

/**
 * UlidRandomFactory class file.
 * 
 * This factory generates ulids that are random for any given millisecond.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Factory\AbstractFactory<UlidInterface>
 */
class UlidRandomFactory extends AbstractFactory implements UlidFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Factory\FactoryInterface::create()
	 * @throws RandomException
	 */
	public function create() : UlidInterface
	{
		try
		{
			return new Ulid(
				(int) (\microtime(true) * 1000.0),
				\random_int(0, 0xFFFFFFFFFF),
				\random_int(0, 0xFFFFFFFFFF),
			);
		}
		catch(Throwable $exc)
		{
			// TODO remove with php8.3+
			throw new RandomException('Wrapped random exception', -1, $exc);
		}
	}
	
}
